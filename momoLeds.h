//
// Logitech MOMO Racing LED control with HID
// By Kegetys <kegetys[�t]dnainternet.net>
//

#ifndef __MOMOLEDS_H__
#define __MOMOLEDS_H__

bool initHID(int devID, int devNum);
bool setLED(bool led1, bool led2);
bool closeHID();

#endif