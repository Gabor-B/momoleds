---------------------------------------------------------
         �:  MOMO Leds v1.3 for LFS S2:�
---------------------------------------------------------

This program makes the two LEDs in the Logitech MOMO/MOMO Racing
wheels light up based on events in Live for Speed S2.

To use, enable OutGauge data sending to localhost from LFS 
by editing the cfg.txt file, for example:
OutGauge Mode 1
OutGauge Delay 1
OutGauge IP 127.0.0.1
OutGauge Port 56551

Then run the LFSmomoLeds.exe executable. If you have the
"red" MOMO, you will need to change the device ID from the
configuration file to 49813.

To configure the LEDs, edit the momoleds.cfg configuration
file. In this file you have the general settings in the
[config] section, and default- and optional car specific
settings for LED actions. 

The different LED actions are listed in the config file,
LED1 is the left led and LED2 is the right led. For each
LED, you can have two actions. For the primary action, set 
with LED1 and LED2, a negative value will negate the LED
state. For the secondary action, set with LED1b and LED2b
a negative value means that a OR logical operation is done
to the primary and secondary LED states. A positive value
means that a XOR logical operation is done.

If no car specifig settings are found the [default] one
is used.

A few examples:

For the LX4, Left LED is a shift light (1) and right LED 
is a low fuel warning light(10) which lights up when
the fuel amount goes below 5%:
[lx4]
LED1=1
LED2=10
lowFuel=5.0


For the MRT5, Left LED is a shift light (1) and right LED 
is a negated redline light (-7), which will be lit when
the engine is NOT over the redline:
[MRT]
LED1=1
LED2=-7


For UFR, left LED lights up when the RPM goes over 8000,
the right LED lights up when the RPM goes over 9500 and 
when the shift light lits up both LEDs begin to flash:
[UFR]
LED1=8000
LED2=9500
LED1b=12
LED2b=12


For FZ5, left LED is a shift light, and the right LED
is a TC LED(11) or will begin flashing when the fuel 
amount remaining suffices for less than 5 laps:
[FZ5]
LED1=1
LED2=11
LED2b=18
lowFuel=5.0


---------------------------------------------------------
  credits
---------------------------------------------------------

Made by Kegetys <kegetys@dnainternet.net> 
V1.2 update by DirkT
V1.3 update by GabbO

---------------------------------------------------------
  license & disclaimer
---------------------------------------------------------

You are permitted to install and use this software for 
personal entertainment purposes only. Any commercial,
military or educational use is strictly forbidden without
permission from the author. 

You are free to distribute this software as you wish, as 
long as it is kept 100% free of charge, it is not modified 
in any way and this readme file is distributed with it.

The author takes no responsibility for any damages this 
program may cause, use at your own risk.

---------------------------------------------------------