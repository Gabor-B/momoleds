//
// Logitech MOMO Racing LED control with HID
// By Kegetys <kegetys[�t]dnainternet.net>
//

// Needs Windows Driver Development Kit
// Link with setupapi.lib, hid.lib

#include <stdio.h>
#include <windows.h>
extern "C" {
	#include <wtypes.h>
	#include <initguid.h>
	#include <hidsdi.h>
	#include <setupapi.h>
}


HANDLE wheelHandle = NULL;
PSP_DEVICE_INTERFACE_DETAIL_DATA        detaild;

// Initialize HID device, devID 0xCA03 = momo racing
bool initHID(int devID, int devNum) {
	if(wheelHandle != NULL) {
		return true;
	}
	printf("initHID: Initializing HID (Device ID 0x%04X)...", devID);
	GUID hg;
	HANDLE di;
	SP_DEVICE_INTERFACE_DATA ddata;
	ddata.cbSize = sizeof(ddata);

	// Get GUID
	HidD_GetHidGuid(&hg);
	printf("HID GUID: %X\n", hg);
	di = SetupDiGetClassDevs(&hg, NULL, NULL, DIGCF_PRESENT|DIGCF_INTERFACEDEVICE);

	int idx = 0;
	bool breakSearch = false;
	do {
		// Enumerate devices
		LONG res = SetupDiEnumDeviceInterfaces(di, 0, &hg, idx, &ddata);
		if(res) {
			// Device found			
			DWORD len = 0, req = 0;

			// Get size
			res = SetupDiGetDeviceInterfaceDetail(di, &ddata, NULL, NULL, &len, NULL);
			detaild = (PSP_DEVICE_INTERFACE_DETAIL_DATA) malloc(len);
			detaild->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);

			// Get actual data
			res = SetupDiGetDeviceInterfaceDetail(di, &ddata, detaild, len, &req, NULL);

			// Open handle
			wheelHandle = CreateFile(detaild->DevicePath, GENERIC_READ|GENERIC_WRITE, FILE_SHARE_READ|FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL);

			// Get attributes
			HIDD_ATTRIBUTES attrib;
			attrib.Size = sizeof(attrib);

			res = HidD_GetAttributes(wheelHandle, &attrib);
			//printf("Device %d: %04X, %04X\n", idx, attrib.VendorID, attrib.ProductID);

			if(attrib.ProductID == devID && attrib.VendorID == 0x046D) {
				// This is the device
				if(devNum == 0) {
					printf("initHID: Device found.\n");
					return true;
				} else {
					devNum--;
				}
			}

			CloseHandle(wheelHandle);
			free(detaild);
			wheelHandle = NULL;
		} else {
			break;
		}
		idx++; // Goto next device
	} while(!breakSearch);

	printf("initHID: Device not found.\n");
	return false;
}

// Set LEDS
#define MOMO_LED1	0x01
#define MOMO_LED2	0x02
bool setLED(bool led1, bool led2) {
	if(wheelHandle == NULL) {
		return false;
	}
	//printf("setLED: %d, %d\n", led1, led2);

	// Write LED packet to wheel
	BYTE foo[]={0x00, 0x09, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}; // Third byte = led bits
	DWORD written;

	if(led1)
		foo[2] += MOMO_LED1; 
	if(led2)
		foo[2] += MOMO_LED2; 
	int ret = WriteFile(wheelHandle, foo, 8, &written, NULL);

	if(written != 8) {
		return false;
	}
	return true;
}

// Close HID device
bool closeHID() {
	if(wheelHandle == NULL) {
		return false;
	}
	CloseHandle(wheelHandle);
	free(detaild);
	wheelHandle = NULL;
	return true;
}
