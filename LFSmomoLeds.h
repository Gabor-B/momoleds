//
// Logitech MOMO Racing LED control with HID
// By Kegetys <kegetys[�t]dnainternet.net>
// Modified by DirkT <dirk[�t]upexia.nl>
//

#ifndef __LFSMOMOLEDS_H__
#define __LFSMOMOLEDS_H__

#include "WinSock2.h"
#include <windows.h>
#include <stdio.h>
#include "momoLeds.h"

#define LED_INVALID			-75
#define LED_OFF				0
#define LED_SHIFTLIGHT		1
#define LED_HANDBRAKE		2
#define LED_PITSPEED		3
#define LED_PITSPEEDFLASH	4
#define LED_SIGNALLEFT		5
#define LED_SIGNALRIGHT		6
#define LED_REDLINE			7
#define LED_OILWARN			8
#define LED_FULLBEAM		9
#define LED_LOWFUEL			10
#define LED_TC				11
#define LED_SHIFTLIGHTFLASH	12
#define LED_LOWFUELFLASH	13
#define LED_ON				14
#define LED_PRE_SHIFTLIGHT1	15
#define LED_PRE_SHIFTLIGHT2	16
#define LED_LOWFUEL_LAPS	17
#define LED_LOWFUELFLASH_LAPS	18
#define LED_RPM				100

#define DEVICE_ID_BLACK		0xCA03		// Black MOMO device ID
#define DEVICE_ID_RED		0xC295		// Red MOMO device ID

// Outgauge
typedef struct {
	unsigned	Time;			// time in milliseconds (to check order)

	char		Car[4];			// Car name
	short		Flags;			// Info (see OG_x below)
	byte		Gear;			// Reverse:0, Neutral:1, First:2...
	byte		PLID;			// Unique ID of viewed player (0 = none)
	float		Speed;			// M/S
	float		RPM;			// RPM
	float		Turbo;			// BAR
	float		EngTemp;		// C
	float		Fuel;			// 0 to 1
	float		OilPressure;	// BAR
	float		OilTemp;		// C
	short		Filler;			// Offset needed for >= Z28 release (Typo in inSim documentation?)
	unsigned	DashLights;		// Dash lights available (see DL_x below)
	unsigned	ShowLights;		// Dash lights currently switched on
	float		Throttle;		// 0 to 1
	float		Brake;			// 0 to 1
	float		Clutch;			// 0 to 1
	char		Display1[16];	// Usually Fuel
	char		Display2[16];	// Usually Settings

	int			ID;				// optional - only if OutGauge ID is specified
} OGPACKET;

bool ogInit();
bool ogRead(OGPACKET *dpck);
void ogClose();

// Other function prototypes
void loadConfig();
void setCurCarId(OGPACKET* pck);
bool getLedState(int act, OGPACKET* pck);
float getRemainingLaps(OGPACKET* pck);
bool getShiftLight(OGPACKET* pck);

#define	DL_SHIFT		1		// bit 0	- shift light
#define DL_FULLBEAM		2		// bit 1	- full beam
#define DL_HANDBRAKE	4		// bit 2	- handbrake
#define DL_PITSPEED		8		// bit 3	- pit speed limiter
#define DL_TC			16		// bit 4	- TC active or switched off
#define DL_SIGNAL_L		32		// bit 5	- left turn signal
#define DL_SIGNAL_R		64		// bit 6	- right turn signal
#define DL_SIGNAL_ANY	128		// bit 7	- shared turn signal
#define DL_OILWARN		256		// bit 8	- oil pressure warning
#define DL_BATTERY		512		// bit 9	- battery warning
#define DL_ABS			1024	// bit 10	- ABS active or switched off
#define DL_SPARE		2048	// bit 11
#define DL_NUM

#define SHIFT_THRESHOLD 0.99
#define PRESHIFT1_THRESHOLD 0.94
#define PRESHIFT2_THRESHOLD 0.965

enum CAR_IDS {CAR_UF1, CAR_XFG, CAR_XRG, CAR_LX4, CAR_LX6, CAR_RB4, CAR_FXO, CAR_XRT, CAR_RAC, CAR_FZ5, 
	CAR_UFR, CAR_XFR, CAR_FXR, CAR_XRR, CAR_FZR, CAR_MRT, CAR_FBM, CAR_FOX, CAR_FO8, CAR_BF1, CAR_END};
extern const char* CAR_ID_NAMES[CAR_END];

#endif