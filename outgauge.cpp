//
// Logitech MOMO Racing LED control with HID
// By Kegetys <kegetys[�t]dnainternet.net>
//

// LFS OutGauge reading
#include "LFSmomoLeds.h"

SOCKET		ogSocket = INVALID_SOCKET;
sockaddr_in	ogSockAddr;
WSADATA			wsaData;

extern int cOutGaugePort;
unsigned short		ogListenPort = 56551;
int					ogID		 = 56551;

bool ogInitialized = false;
// Init UDP if not already initialized
bool ogInit() {
	if(ogInitialized)
		return true;

	printf("ogInit: Initializing OutGauge, port %d...\n", cOutGaugePort);

	if(WSAStartup(0x202, &wsaData) == SOCKET_ERROR)	{
		printf("ogInit : WSAStartup failed");
		WSACleanup(); 
		return 0;
	}

	ogSockAddr.sin_family = AF_INET;
	ogSockAddr.sin_addr.s_addr = INADDR_ANY;

	ogSockAddr.sin_port = htons(cOutGaugePort); 
	ogSocket = socket(AF_INET, SOCK_DGRAM, 0);		// create UDP socket 
	if(ogSocket == INVALID_SOCKET) {
		printf("ogInit: Socket creation failed\n");
		WSACleanup(); 
		return false;
	}

	// Bind it
	if(bind(ogSocket,(sockaddr*)&ogSockAddr, sizeof(ogSockAddr)) == SOCKET_ERROR) {
		printf("ogInit: Socket bind failed\n");
		closesocket(ogSocket);
		ogSocket = INVALID_SOCKET;
		WSACleanup(); 
		return false;
	}

	HANDLE hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	if(WSAEventSelect(ogSocket, hEvent, FD_READ|FD_CLOSE) == SOCKET_ERROR) {
		printf("ogInit: WSAEventSelect failed\n");
		closesocket(ogSocket);
		ogSocket = INVALID_SOCKET;
		WSACleanup(); 
		return false;
	}

	ogInitialized = true;
	return true;
}

void ogClose() {
	if(ogInitialized) {
		return;
	}
	printf("ogClose: Closing OutGauge...\n");

	closesocket(ogSocket);
	WSACleanup();
}

// Attempt to read data & fill packet
bool ogRead(OGPACKET *dpck) {
	if(ogSocket == INVALID_SOCKET)
		return false;

	dpck->ID = -1;
	bool cr = true;
	bool valid = false;
	int retval;
	WSANETWORKEVENTS ne;
	while(cr) {
		memset(&ne, 0, sizeof(ne));	
		if(WSAEnumNetworkEvents(ogSocket, NULL, &ne) == SOCKET_ERROR)
			cr = 0;
		else {
			if(ne.lNetworkEvents & FD_READ) {
				// Read data until no more
				OGPACKET tpck;
				retval = recv(ogSocket, (char*) &tpck, sizeof(OGPACKET), 0);
				if(retval > 0) {
					// Valid data					
					memcpy(dpck, &tpck, sizeof(OGPACKET));
					valid = true;
				} else if(tpck.ID != ogID) {
					printf("ogRead: received invalid data (%d != %d, %d)\n", dpck->ID, ogID, retval);
				}
			} else
				cr = 0;
		}
	}
	return valid;
}