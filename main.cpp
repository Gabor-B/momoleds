//
// Logitech MOMO Racing LED control with HID
// By Kegetys <kegetys[�t]dnainternet.net>
// Modified by DirkT <dirk[�t]upexia.nl>
// Update for 0.6G by GabbO
//

#include "LFSmomoLeds.h"
#include <regex>

const char* CAR_ID_NAMES[] = {"UF1", "XFG", "XRG", "LX4", "LX6", "RB4", "FXO", "XRT", "RAC", 
	"FZ5", "UFR", "XFR", "FXR", "XRR", "FZR", "MRT", "FBM", "FOX", "FO8", "BF1"};
const short CAR_MAX_RPM[] = {7000, 8000, 7000, 9000, 9000, 7500, 7500, 7500, 7000, 
	8000, 9000, 8000, 7500, 7500, 8500, 13000, 9200, 7500, 9500, 19912};

std::regex fuelPattern ("\\d+\\.\\d{1}L");

// Config settings
int cDeviceID = DEVICE_ID_RED;
int cDeviceNum = 0;
int cOutGaugePort = 0;
DWORD cPitSpeedFlashtime = 250;
DWORD cShiftLightFlashtime = 200;
DWORD cLowFuelFlashtime = 1000;

char curCar[4];
byte curCarId;
int aLED1 = 0; // current car LED action (if curCarCustom == true)
int aLED2 = 0;
int aLED1b, aLED2b;
float lowFuel = 0.0f;

// Shutdown
void shutdown() {
	ogClose();
	closeHID();
}

int __stdcall breakHandler(DWORD type) {
	switch(type) {
		case CTRL_CLOSE_EVENT:
		case CTRL_BREAK_EVENT:
		case CTRL_C_EVENT:
		case CTRL_LOGOFF_EVENT:
		case CTRL_SHUTDOWN_EVENT:
			// shut everything down
			printf("Shutdown...\n");
			shutdown();

			Sleep(200);			
			break;
	}

	return FALSE;
}

void main(void) {
	printf("---------------------------------\n");
	printf(" LFS MOMO LEDs v1.3 by Kegetys\n");
	printf("  modified by DirkT and GabbO\n");
	printf("---------------------------------\n\n");

	SetConsoleTitle("MOMO LEDs");	
	SetConsoleCtrlHandler(breakHandler, true);
	loadConfig();
	ZeroMemory(curCar, 4);

	// Init OutGauge
	if(!ogInit()) {
		printf("OutGauge init failed!\n");
		shutdown();
	}

	// Init HID device
	if(!initHID(cDeviceID, cDeviceNum)) {
		printf("HID init failed!\n");
		shutdown();
	}

	printf("Ready for OutGauge data\n");
	int lastValidPacket = 0;
	while(true) {
		// Read OutGauge packet
		OGPACKET pck;
		if(ogRead(&pck)) {
			/*printf("LED_SHIFTLIGHT: %d\n", pck.DashLights&DL_SHIFT);
			printf("LED_HANDBRAKE: %d\n", pck.DashLights&DL_HANDBRAKE);
			printf("LED_PITSPEED: %d\n", pck.DashLights&DL_PITSPEED);
			printf("LED_TC: %d\n", pck.DashLights&DL_TC);
			printf("LED_SIGNALLEFT: %d\n", pck.DashLights&DL_SIGNAL_L);
			printf("LED_SIGNALRIGHT: %d\n", pck.DashLights&DL_SIGNAL_R);
			printf("LED_SIGNAL_ANY: %d\n", pck.DashLights&DL_SIGNAL_ANY);
			printf("LED_OILWARN: %d\n", pck.DashLights&DL_OILWARN);
			printf("LED_OILWARN: %d\n", pck.DashLights&DL_OILWARN);
			printf("Fuel: %f\n", pck.Fuel);
			printf("Display1:%s, Display2:%s\n", pck.Display1, pck.Display2);
			printf("RPM: %f\n", pck.RPM);
			*/

			if(strncmp(curCar, pck.Car, 4)) {
				// Car changed, load custom setting for car (or default)
				strncpy_s(curCar, pck.Car, 4);
				setCurCarId(&pck);
				loadConfig();
			}
			
			bool led1 = getLedState(aLED1, &pck);
			bool led2 = getLedState(aLED2, &pck);
			bool led1b = getLedState(aLED1b, &pck);
			bool led2b = getLedState(aLED2b, &pck);

			// If negative values, negate the led state
			if(aLED1 < 0) {led1 = !led1;}
			if(aLED2 < 0) {led2 = !led2;}
			
			setLED(aLED1b<0?led1|led1b:led1^led1b, aLED2b<0?led2|led2b:led2^led2b);

			lastValidPacket = GetTickCount();
		}

		if(GetTickCount() - lastValidPacket > 2000) {
			// No data for 2 seconds, turn lights off and idle a bit more
			setLED(false, false);
			Sleep(100);
		}
		Sleep(10);
	}

	shutdown();
}

bool loadedConfig = false;
void loadConfig() {
	// Load config settings from ini
	static char path[512];
	char temp[128];
	if(!loadedConfig) {
		GetCurrentDirectory(sizeof(path), (char*) &path);
		strcat_s(path, "\\momoleds.cfg");
		printf("Loading config %s...\n", path);
		cOutGaugePort = GetPrivateProfileInt("config", "outGaugePort", 56551, path);
		cDeviceID = GetPrivateProfileInt("config", "deviceID", DEVICE_ID_RED, path);
		cDeviceNum = GetPrivateProfileInt("config", "deviceNum", 0, path);
		cPitSpeedFlashtime = GetPrivateProfileInt("config", "speedlimitFlashTime", 200, path);
		cShiftLightFlashtime = GetPrivateProfileInt("config", "shiftlightFlashTime", 200, path);
		cLowFuelFlashtime = GetPrivateProfileInt("config", "lowfuelFlashTime", 1000, path);	
		
		loadedConfig = true;
		return;
	}

	printf("Car: %s\n", curCar);

	// Check for car-specifig setting
	int cl1 = GetPrivateProfileInt(curCar, "LED1", LED_INVALID, path);
	int cl2 = GetPrivateProfileInt(curCar, "LED2", LED_INVALID, path);
	int cl1b = GetPrivateProfileInt(curCar, "LED1b", LED_INVALID, path);
	int cl2b = GetPrivateProfileInt(curCar, "LED2b", LED_INVALID, path);

	GetPrivateProfileString(curCar, "lowFuel", "-1.0", (LPSTR) &temp, 128, path);
	float fl = (float) atof(temp);

	// Use default if not found
	if(cl1 == LED_INVALID) {
		aLED1 = GetPrivateProfileInt("default", "LED1", LED_OFF, path);
	} else {
		aLED1 = cl1;
	}
	if(cl2 == LED_INVALID) {
		aLED2 = GetPrivateProfileInt("default", "LED2", LED_OFF, path);
	} else {
		aLED2 = cl2;
	}
	if(cl1b == LED_INVALID) aLED1b = GetPrivateProfileInt("default", "LED1b", LED_OFF, path);
	else aLED1b = cl1b;
	if(cl2b == LED_INVALID) aLED2b = GetPrivateProfileInt("default", "LED2b", LED_OFF, path);
	else aLED2b = cl2b;

	if(fl < 0) {
		GetPrivateProfileString("default", "lowFuel", "5.0", (LPSTR) &temp, 128, path);
		lowFuel = (float) atof(temp);
	} else {
		lowFuel = fl;
	}

}

// Return LED state based on action & outgauge state
bool getLedState(int act, OGPACKET* pck) {
	if(act < 0) {// Negate if negative
		act = -act;
	}
	switch(act) {
		case LED_OFF:
			return FALSE;
		case LED_ON:
			return TRUE;
		case LED_PITSPEED:
			return (pck->DashLights&DL_PITSPEED) != 0;
		case LED_PITSPEEDFLASH: // Flashing pitspeed limiter
			if(pck->DashLights&DL_PITSPEED) {
				return (((GetTickCount()%(cPitSpeedFlashtime*2))-cPitSpeedFlashtime) > cPitSpeedFlashtime);
			} else {
				return FALSE;
			}
		case LED_PRE_SHIFTLIGHT1:	// shift light1 before shift limit
			return (pck->RPM >= PRESHIFT1_THRESHOLD * CAR_MAX_RPM[curCarId]);
		case LED_PRE_SHIFTLIGHT2:	// shift light2 before shift limit
			return (pck->RPM >= PRESHIFT2_THRESHOLD * CAR_MAX_RPM[curCarId]);
		case LED_SHIFTLIGHT:
			return getShiftLight(pck);
		case LED_SHIFTLIGHTFLASH: // Flashing shiftlight
			if (getShiftLight(pck)) {
				return (((GetTickCount()%(cShiftLightFlashtime*2))-cShiftLightFlashtime) > cShiftLightFlashtime);
			} else {
				return FALSE;
			}
		case LED_HANDBRAKE:
			return (pck->DashLights&DL_HANDBRAKE) != 0;
		case LED_TC:
			return (pck->DashLights&DL_TC) != 0;			
		case LED_SIGNALLEFT:
			return (pck->DashLights&DL_SIGNAL_L || pck->DashLights&DL_SIGNAL_ANY) != 0;
		case LED_SIGNALRIGHT:
			return (pck->DashLights&DL_SIGNAL_R || pck->DashLights&DL_SIGNAL_ANY) != 0;
		case LED_REDLINE:
			return FALSE; // Removed (Deprecated)
		case LED_OILWARN:
			return (pck->DashLights&DL_OILWARN) != 0;
		case LED_FULLBEAM:
			return (pck->DashLights&DL_FULLBEAM) != 0;
		case LED_LOWFUEL: // Low fuel warning light
			return (pck->Fuel < lowFuel);
		case LED_LOWFUELFLASH: // Flashing low fuel warning light by percentage
			if(pck->Fuel < lowFuel) {
				return (((GetTickCount()%(cLowFuelFlashtime*2))-cLowFuelFlashtime) > cLowFuelFlashtime);
			} else {
				return false;
			}
		case LED_LOWFUEL_LAPS: // Low fuel warning light by laps remaining
			return (getRemainingLaps(pck) < lowFuel);
		case LED_LOWFUELFLASH_LAPS: // Flashing low fuel warning light by laps remaining
			if(getRemainingLaps(pck) < lowFuel) {
				return (((GetTickCount()%(cLowFuelFlashtime*2))-cLowFuelFlashtime) > cLowFuelFlashtime);
			} else {
				return false;
			}
		default:
			if(act > LED_RPM) {
				// RPM based led
				if(pck->RPM > act) {
					return TRUE;
				} else {
					return FALSE;
				}
			}
			printf("Unkown LED action %d\n", act);
			break;
	}
	return FALSE;
}

void setCurCarId(OGPACKET* pck) {
	for (int index = 0; index < CAR_END; index++) {
		if (strncmp(pck->Car, CAR_ID_NAMES[index], 4) == 0) {
			//printf("setting car Id %d for car %s\n", index, pck->Car);
			curCarId = index;
			return;
		}
	}
	 
}

float getRemainingLaps(OGPACKET* pck) {
	std::smatch match;
	std::string s = pck->Display1;
	std::regex_search(s, match, fuelPattern);
	if (match.size() > 0) {
		//printf("Match: %s -> %f\n", match.str().c_str(), atof(match.str().c_str()));
		return (float) atof(match.str().c_str());
	} else {
		return 99;
	}
}

bool getShiftLight(OGPACKET* pck) {
	if (pck->ShowLights&DL_SHIFT) {	  // in case the car has shift lights
		return pck->DashLights&DL_SHIFT;
	} else {
		return pck->RPM >= SHIFT_THRESHOLD * CAR_MAX_RPM[curCarId];
	}
}